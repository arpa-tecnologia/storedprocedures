DELIMITER $$
CREATE   PROCEDURE `SP_Cliente` (IN pta INT, IN pid INT, IN ppn VARCHAR(100), IN psn VARCHAR(100), IN pap VARCHAR(100), IN pam VARCHAR(100), IN pcor VARCHAR(100), IN pcon VARCHAR(100), IN pdca VARCHAR(100), IN pdni NUMERIC(10), IN pdne NUMERIC(10), IN pdcol VARCHAR(100), IN pdmun VARCHAR(100), IN pdes VARCHAR(100), IN pdpa VARCHAR(100), IN pdcp NUMERIC(10), IN pfb TINYINT, IN pgm TINYINT, IN prec TINYINT, IN pfal DATETIME, IN pfba DATETIME, IN pes TINYINT)
BEGIN
	DECLARE vartipoaccion 		INT;
	DECLARE varid 				INT;
	DECLARE varprimernombre 	VARCHAR(100);
	DECLARE varsegundonombre 	VARCHAR(100);
	DECLARE varapellidopaterno 	VARCHAR(100);
	DECLARE varapellidomaterno 	VARCHAR(100);
	DECLARE varcorreo 			VARCHAR(100);
	DECLARE varcontrasena 		VARCHAR(100);
	DECLARE vardomcalle 		VARCHAR(100);
	DECLARE vardomnumeroint 	NUMERIC(10);
	DECLARE varcomnumeroext 	NUMERIC(10);
	DECLARE vardomcolonia 		VARCHAR(100);
	DECLARE vardommunicipio 	VARCHAR(100);
	DECLARE vardomestado 		VARCHAR(100);
	DECLARE vardompais 			VARCHAR(100);
	DECLARE vardomcp 			NUMERIC(10);
	DECLARE varbfacebook 		TINYINT;
	DECLARE varbgmail 			TINYINT;
	DECLARE varrecomendado 		TINYINT;
	DECLARE varfechaalta 		DATETIME;
	DECLARE varfechabaja 		DATETIME;
  DECLARE varestado			TINYINT;

  SET vartipoaccion = pta;
  SET varid = pid;
	SET varprimernombre = ppn;
	SET varsegundonombre = psn;
	SET varapellidopaterno = pap;
	SET varapellidomaterno = pam;
	SET varcorreo = pcor;
	SET varcontrasena = pcon;
	SET vardomcalle = pdca;
	SET vardomnumeroint = pdni;
	SET varcomnumeroext = pdne;
	SET vardomcolonia = pdcol;
	SET vardommunicipio = pdmun;
	SET vardomestado = pdes;
	SET vardompais = pdpa;
	SET vardomcp = pdcp;
	SET varbfacebook = pfb;
	SET varbgmail = pgm;
	SET varrecomendado = prec;
	SET varfechaalta = pfal;
	SET varfechabaja = pfba;
    SET varestado = pes;

    CASE
		WHEN vartipoaccion = 1 THEN
			INSERT INTO tblcliente (cteid, cteprimernombre, ctesegundonombre, cteapellidopaterno, cteapellidomaterno, ctecorreo, ctecontrasena, ctedomcalle, ctedomnumeroint, ctedomnumeroext, ctedomcolonia, ctedommunicipio, ctedomestado, ctedompais, ctedomcp, ctebfacebook, ctebgmail, cterecomendado, ctefechaalta, ctefechabaja, cteestado)
            VALUES (DEFAULT, varprimernombre, varsegundonombre, varapellidopaterno, varapellidomaterno, varcorreo, varcontrasena, vardomcalle, vardomnumeroint, varcomnumeroext, vardomcolonia, vardommunicipio, vardomestado, vardompais, vardomcp, varbfacebook, varbgmail, varrecomendado, varfechaalta, varfechabaja, varestado);
		WHEN vartipoaccion = 2 THEN
			UPDATE tblcliente
            SET
				tblcliente.cteprimernombre =	COALESCE(ppn, tblcliente.cteprimernombre),
				tblcliente.ctesegundonombre =	COALESCE(psn, tblcliente.ctesegundonombre),
				tblcliente.cteapellidopaterno =	COALESCE(pap, tblcliente.cteapellidopaterno),
				tblcliente.cteapellidomaterno =	COALESCE(pam, tblcliente.cteapellidomaterno),
				tblcliente.ctecorreo =	COALESCE(pcor, tblcliente.ctecorreo),
				tblcliente.ctecontrasena =	COALESCE(pcon, tblcliente.ctecontrasena),
				tblcliente.ctedomcalle =	COALESCE(pdca, tblcliente.ctedomcalle),
				tblcliente.ctedomnumeroint =	COALESCE(pdni, tblcliente.ctedomnumeroint),
				tblcliente.ctecomnumeroext =	COALESCE(pdne, tblcliente.ctedomnumeroext),
				tblcliente.ctedomcolonia =	COALESCE(pdcol, tblcliente.ctedomcolonia),
				tblcliente.ctedommunicipio =	COALESCE(pdmun, tblcliente.ctedommunicipio),
				tblcliente.ctedomestado =	COALESCE(pdes, tblcliente.ctedomestado),
				tblcliente.ctedompais =	COALESCE(pdpa, tblcliente.ctedompais),
				tblcliente.ctedomcp =	COALESCE(pdcp, tblcliente.ctedomcp),
				tblcliente.ctebfacebook =	COALESCE(pfb, tblcliente.ctebfacebook),
				tblcliente.ctebgmail =	COALESCE(pgm, tblcliente.ctebgmail),
				tblcliente.cterecomendado =	COALESCE(prec, tblcliente.cterecomendado),
				tblcliente.ctefechaalta =	COALESCE(pfal, tblcliente.ctefechaalta),
				tblcliente.ctefechabaja =	COALESCE(pfba, tblcliente.ctefechabaja)
			WHERE tblcliente.cteid = varid;
        WHEN vartipoaccion = 3 THEN
			UPDATE tblcliente
            SET tblcliente.cteestado = 0
            WHERE tblcliente.cteid = varid;
        WHEN vartipoaccion = 4 THEN
			SELECT * FROM tblcliente
			WHERE tblcliente.cteid = varid;
			WHEN vartipoaccion = 5 THEN
		SELECT * FROM tblcliente;
        ELSE SELECT "ERROR";


	END CASE;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_Configuracion`(IN pta INT, IN psta INT, IN aid int, IN acid int, IN auid int, IN arpid int, IN aab int, IN rpid int, IN rprid int, IN rppid int, IN rpes TINYINT, IN rid int, IN rno varchar(100), IN res TINYINT, IN pid int, IN pno varchar (100), IN pes TINYINT)
BEGIN
	DECLARE vartipoaccion		int;
    DECLARE varsubtipoaccion	int;

    DECLARE varaaccesoid 		int;
	DECLARE varacteid 			int;
	DECLARE varausrid 			int;
	DECLARE vararolpermisoid 	int;
	DECLARE varaaccesobandera 	tinyint;

    DECLARE varrprolpermisoid 	int;
	DECLARE varrprolid 			int;
	DECLARE varrppermisoid 		int;
    DECLARE varrpestado			tinyint;

    DECLARE varrrolid 			int;
	DECLARE varrrolnombre 		varchar(100);
    DECLARE varrrolestado		tinyint;

    DECLARE varppermisoid 		int;
	DECLARE varppermisonombre 	varchar(100);
    DECLARE varppermestado		tinyint;



    SET vartipoaccion = pta;
    SET varsubtipoaccion = psta;
    SET varaaccesoid = aid;
	SET varacteid = acid;
	SET varausrid = auid;
	SET vararolpermisoid = arpid;
	SET varaaccesobandera = aab;

    SET varrprolpermisoid = rpid;
	SET varrprolid = rprid;
	SET varrppermisoid = rppid;
    SET varrpestado = rpes;

    SET varrrolid = rid;
	SET varrrolnombre = rno;
    SET varrrolestado = res;

    SET varppermisoid = pid;
	SET varppermisonombre = pno;
    SET varppermestado = pes;


    CASE
		WHEN vartipoaccion = 1 THEN /*accesos*/
			CASE
				WHEN varsubtipoaccion = 1 THEN
					INSERT INTO tblaccesos
                    VALUES (DEFAULT, varacteid, varausrid, vararolpermisoid, varaaccesobandera);
				WHEN varsubtipoaccion = 2 THEN
					UPDATE tblaccesos
                    SET
						tblaccesos.cteid = COALESCE(varacteid, tblaccesos.cteid),
						tblaccesos.usrid = COALESCE(varausrid, tblaccesos.usrid),
						tblaccesos.rolpermisoid = COALESCE(vararolpermisoid, tblaccesos.rolpermisoid),
						tblaccesos.accesobandera = COALESCE(varaaccesobandera, tblaccesos.accesobandera)
					WHERE tblaccesos.accesoid = varaaccesoid;
				WHEN varsubtipoaccion = 3 THEN
					UPDATE tblaccesos
                    SET tblaccesos.accesobandera = 0
                    WHERE tblaccesos.accesoid = varaaccesoid;
				WHEN varsubtipoaccion = 4 THEN
					SELECT * FROM tblaccesos
					WHERE tblaccesos.accesoid = varaaccesoid;
					WHEN varsubtipoaccion = 5 THEN
						SELECT * FROM tblaccesos;
			END CASE;
		WHEN vartipoaccion = 2 THEN /*rol permiso*/
			CASE
				WHEN varsubtipoaccion = 1 THEN
					INSERT INTO tblrolepermiso
                    VALUES (DEFAULT, varrprolid, varrppermisoid, varrpestado);
				WHEN varsubtipoaccion = 2 THEN
					UPDATE tblrolepermiso
                    SET
						tblrolepermiso.rolid = COALESCE(varrprolid, tblrolepermiso.rolid),
                        tblrolepermiso.permisoid = COALESCE(varrppermisoid, tblrolepermiso.permisoid)
					WHERE tblrolepermiso.rolpermisoid = varrprolpermisoid;
				WHEN varsubtipoaccion = 3 THEN
					UPDATE tblrolepermiso
                    SET tblrolepermiso.rpestado = 0
                    WHERE tblrolepermiso.rolpermisoid = varrprolpermisoid;
                WHEN varsubtipoaccion = 4 THEN
					SELECT * FROM tblrolepermiso
					WHERE tblrolepermiso.rolpermisoid = varrprolpermisoid;
					WHEN varsubtipoaccion = 5 THEN
		SELECT * FROM tblrolepermiso;
			END CASE;
		WHEN vartipoaccion = 3 THEN /*rol*/
			CASE
				WHEN varsubtipoaccion = 1 THEN
					INSERT INTO tblrol
					VALUES (DEFAULT, varrrolnombre, varrrolestado);
				WHEN varsubtipoaccion = 2 THEN
					UPDATE tblrol
                    SET tblrol.rolnombre = COALESCE(varrrolnombre, tblrol.rolnombre)
                    WHERE tblrol.rolid = varrrolid;
				WHEN varsuptipoaccion = 3 THEN
					UPDATE tblrol
                    SET tblrol.rolestado = 0
                    WHERE tblrol.rolid = varrrolid;
				WHEN varsubtipoaccion = 4 THEN
					SELECT * FROM tblrol
					WHERE tblrol.rolid = varrrolid;
					WHEN varsubtipoaccion = 5 THEN
						SELECT * FROM tblrol;
            END CASE;
		WHEN vartipoaccion = 4 THEN /*permiso*/
			CASE
				WHEN varsubtipoaccion = 1 THEN
					INSERT INTO tblpermisos
                    VALUES (DEFAULT, varppermisonombre, varppermestado);
				WHEN varsubtipoaccion = 2 THEN
					UPDATE tblpermisos
                    SET tblpermisos.permnombre = COALESCE(varppermisonombre, tblpermisos.permnombre)
                    WHERE tblpermisos.permisoid = varppermisoid;
				WHEN varsubtipoaccion = 3 THEN
					UPDATE tblpermisos
                    SET tblpermisos.permestado = 0
                    WHERE tblpermisos.permisoid = varppermisoid;
				WHEN varsubtipoaccion = 4 THEN
					SELECT * FROM tblpermisos
					WHERE tblpermisos.permisoid = varppermisoid;
					WHEN varsubtipoaccion = 5 THEN
						SELECT * FROM tblpermisos;
			END CASE;
    END CASE;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_Empresa`(IN pta INT, IN pid INT, IN pgid INT, IN plid INT, IN pti BOOL, IN prs VARCHAR(100), IN prfc VARCHAR(50), IN pdca VARCHAR(100), IN pdni INT, IN pdne INT, IN pdcol VARCHAR(100), IN pdmun VARCHAR(100), IN pdes VARCHAR(100), IN pdpa VARCHAR(100), IN pdcp INT, IN ploc TEXT, IN plog VARCHAR(100), IN pes TINYINT, IN pfal DATETIME, IN pfm DATETIME)
BEGIN
	DECLARE vartipoaccion 			INT;
	DECLARE varid 					INT;
	DECLARE vargiroid 				INT;
	DECLARE varlicenciaid 			INT;
	DECLARE vartipo 				BOOL;
	DECLARE varrazonsocial 			VARCHAR(100);
	DECLARE varrfc 					VARCHAR(50);
	DECLARE vardomcalle 			VARCHAR(100);
	DECLARE vardomnumeroint 		INT;
	DECLARE vardomnumeroext 		INT;
	DECLARE vardomcolonia  			VARCHAR(100);
	DECLARE vardommunicipio 		VARCHAR(100);
	DECLARE vardomestado 			VARCHAR(100);
	DECLARE vardompais  			VARCHAR(100);
	DECLARE vardomcp 				INT;
	DECLARE varlocalizacion 		TEXT;
	DECLARE varlogo 				VARCHAR(100);
	DECLARE varfechaalta 			DATETIME;
	DECLARE varfechamodificacion 	DATETIME;
    DECLARE varestado				TINYINT;

		SET vartipoaccion = pta;
    SET varid = pid;
	SET vargiroid = pgid;
	SET varlicenciaid = plid;
	SET vartipo = pti;
	SET varrazonsocial = prs;
	SET varrfc = prfc;
	SET vardomcalle = pdca;
	SET vardomnumeroint = pdni;
	SET vardomnumeroext = pdne;
	SET vardomcolonia = pdcol;
	SET vardommunicipio = pdmun;
	SET vardomestado = pdes;
	SET vardompais = pdpa;
	SET vardomcp = pdcp;
	SET varlocalizacion = ploc;
	SET varlogo = plog;
	SET varfechaalta = pfal;
	SET varfechamodificacion = pfm;
    SET varestado = pes;

    CASE
		WHEN vartipoaccion = 1 THEN
			INSERT INTO tblempresa
            VALUES (DEFAULT, vargiroid, varlicenciaid, vartipo, varrazonsocial, varrfc, vardomcalle, vardomnumeroint, vardomnumeroext, vardomcolonia, vardommunicipio, vardomestado, vardompais, vardomcp, varlocalizacion, varlogo, varestado, varfechaalta, varfechamodificacion);
		WHEN vartipoaccion = 2 THEN
			UPDATE tblempresa
            SET
				tblempresa.giroid = COALESCE(pgid, tblempresa.giroid),
				tblempresa.licenciaid = COALESCE(plid, tblempresa.licenciaid),
				tblempresa.emptipo = COALESCE(pti, tblempresa.emptipo),
				tblempresa.emprazonsocial = COALESCE(prs, tblempresa.emprazonsocial),
				tblempresa.emprfc = COALESCE(prfc, tblempresa.emprfc),
				tblempresa.empdomcalle = COALESCE(pdca, tblempresa.empdomcalle),
				tblempresa.empdomnumeroint = COALESCE(pdni, tblempresa.empdomnumeroint),
				tblempresa.empdomnumeroext = COALESCE(pdne, tblempresa.empdomnumeroext),
				tblempresa.empdomcolonia = COALESCE(pdcol, tblempresa.empdomcolonia),
				tblempresa.empdommunicipio = COALESCE(pdmun, tblempresa.empdommunicipio),
				tblempresa.empdomestado = COALESCE(pdes, tblempresa.empdomestado),
				tblempresa.empdompais = COALESCE(pdpa, tblempresa.empdompais),
				tblempresa.empdomcp = COALESCE(pdcp, tblempresa.empdomcp),
				tblempresa.emplocalizacion = COALESCE(ploc, tblempresa.emplocalizacion),
				tblempresa.emplogo = COALESCE(plog, tblempresa.emplogo),
				tblempresa.empfechaalta = COALESCE(pfal, tblempresa.empfechaalta),
				tblempresa.empfechamodificacion = COALESCE(pfm, tblempresa.empfechamodificacion)
			WHERE tblempresa.empid = varid;
		WHEN vartipoaccion = 3 THEN
			UPDATE tblempresa
			SET tblempresa.empestado = 0
			WHERE tblempresa.empid = varid;
		WHEN vartipoaccion = 4 THEN
			SELECT * FROM tblempresa
			WHERE tblempresa.empid = varid;
			WHEN vartipoaccion = 5 THEN
				SELECT * FROM tblempresa;
		ELSE SELECT "ERROR";
	END CASE;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_Encuestas`(IN pta INT, IN pid INT, IN paid INT, IN pfi DATETIME, IN pff DATETIME, IN pme FLOAT(2,2), IN pumm FLOAT(2,2), IN ppdm TEXT, IN pfal DATETIME, IN pfac DATETIME, IN paida INT, IN pfb DATETIME, IN pes TINYINT, IN pempid INT, IN pnom VARCHAR(100))
BEGIN
DECLARE vartipoaccion 			INT;
	DECLARE varid 					INT;
	DECLARE varaccesoid 			INT;
	DECLARE varfechainicio 			DATETIME;
	DECLARE varfechafin 			DATETIME;
	DECLARE varmeta 				FLOAT(2,2);
	DECLARE varubicacionmaxmin 		FLOAT(2,2);
	DECLARE varplandemejora 		TEXT;
	DECLARE varfechaalta 			DATETIME;
	DECLARE varfechaactualiza 		DATETIME;
	DECLARE varaaccesoidactualiza 	INT;
	DECLARE varfechabaja 			DATETIME;
    DECLARE varestado				TINYINT;
		DECLARE varempresaid 				INT;
		DECLARE varnombre 					INT;

    SET vartipoaccion = pta;
    SET varid = pid;
	SET varaccesoid = paid;
	SET varfechainicio = pfi;
	SET varfechafin = pff;
	SET varmeta = pme;
	SET varubicacionmaxmin = pumm;
	SET varplandemejora = ppdm;
	SET varfechaalta = pfal;
	SET varfechaactualiza = pfac;
	SET varaaccesoidactualiza = paida;
	SET varfechabaja = pfb;
    SET varestado = pes;
		SET varempresaid = pempid;
		SET varnombre = pnom;

    CASE
		WHEN vartipoaccion = 1 THEN
			INSERT INTO tblencuestas
            VALUES (DEFAULT, varaccesoid, varfechainicio, varfechafin, varmeta, varubicacionmaxmin, varplandemejora, varfechaalta, varfechaactualiza, varaaccesoidactualiza, varfechabaja, varestado, varempresaid, varnombre);
		WHEN vartipoaccion = 2 THEN
			UPDATE tblencuestas
            SET
				tblencuestas.encaccesoid = COALESCE(paid, tblencuestas.encaccesoid),
				tblencuestas.encfechainicio = COALESCE(pfi, tblencuestas.encfechainicio),
				tblencuestas.encfechafin = COALESCE(pff, tblencuestas.encfechafin),
				tblencuestas.encmeta = COALESCE(pme, tblencuestas.encmeta),
				tblencuestas.encubicacionmaxmin = COALESCE(pumm, tblencuestas.encubicacionmaxmin),
				tblencuestas.encplandemejora = COALESCE(ppdm, tblencuestas.encplandemejora),
				tblencuestas.encfechaalta = COALESCE(pfal, tblencuestas.encfechaalta),
				tblencuestas.encfechaactualiza = COALESCE(pfac, tblencuestas.encfechaactualiza),
				tblencuestas.encaaccesoidactualiza = COALESCE(paida, tblencuestas.encaaccesoidactualiza),
				tblencuestas.encfechabaja = COALESCE(pfb, tblencuestas.encfechabaja),
				tblencuestas.encempid = COALESCE(pempid, tblencuestas.encempid),
				tblencuestas.encnombre = COALESCE(pnom, tblencuestas.encnombre)
			WHERE tblencuaestas.encid = varid;
		WHEN vartipoaccion = 3 THEN
			UPDATE tblencuestas
            SET tblencuestas.encestado = 0
            WHERE tblencuestas.encid = varid;
		WHEN vartipoaccion = 4 THEN
			SELECT * FROM tblencuestas
			WHERE tblencuestas.encid = varid;
			WHEN vartipoaccion = 5 THEN
				SELECT * FROM tblencuestas;
		ELSE SELECT "ERROR";
		END CASE;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_Estadisticas`(IN peid INT)
BEGIN
	DECLARE varencuestaid INT;

    SET varencuestaid = peid;

    SELECT prpregid AS idPregunta, pregtitulo, pregevalua, TRUNCATE(AVG(resppuntos), 2) AS Promedio, COUNT(*) AS NumRespuestas, MONTHNAME(logfecha) as mes, resptitulo, resppuntos
    FROM tblpregresp
    JOIN tblpreguntas ON tblpregresp.prpregid = tblpreguntas.pregid
    JOIN tblrespuestas ON tblpregresp.prrespid = tblrespuestas.respid
    JOIN tblencuestas ON tblpregresp.prencuestaid = tblencuestas.encid
    JOIN tbllogencuesta ON tblpregresp.prcteid = tbllogencuesta.logcteid
    WHERE tblpreguntas.pregencuestaid = varencuestaid
    GROUP BY prpregid, pregtitulo, pregevalua, mes
    ORDER BY  logfecha, prpregid ASC;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_LogEncuestas`(IN pta INT, IN pid int, IN pcid int, IN peid int, IN pnid int, IN pavid int, IN pacid int, IN ppid int, IN pipid int, IN ppa int, IN pfe float, IN pev datetime, IN plo float, IN plm text)
BEGIN
	DECLARE vartipoaccion			int;
	DECLARE varid			 		int;
	DECLARE varcteid			 	int;
	DECLARE varencuestaid			int;
	DECLARE varnivelid			 	int;
	DECLARE varavatarid			 	int;
	DECLARE varaccesorioid			int;
	DECLARE varpromoid			 	int;
	DECLARE variconopreguntaid		int;
	DECLARE varpuntosacum			float(2,2);
	DECLARE varfecha				datetime;
	DECLARE varevaluacion			float(2,2);
	DECLARE varlocalizacion			text;
	DECLARE varlocalizacionmins		datetime;

    SET vartipoaccion = pta;
    SET varid = pid;
	SET varcteid = pcid;
	SET varencuestaid = peid;
	SET varnivelid = pnid;
	SET varavatarid = pavid;
	SET varaccesorioid = pacid;
	SET varpromoid = ppid;
	SET variconopreguntaid = pipid;
	SET varpuntosacum = ppa;
	SET varfecha = pfe;
	SET varevaluacion = pev;
	SET varlocalizacion = plo;
	SET varlocalizacionmins = plm;

    CASE
		WHEN vartipoaccion = 1 THEN
			INSERT INTO tbllogencuesta
            VALUES (DEFAULT, varcteid, varencuestaid, varnivelid, varavatarid, varaccesorioid, varpromoid, variconopreguntaid, varpuntosacum, varfecha, varevaluacion, varlocalizacion, varlocalizacionmins);
		WHEN vartipoaccion = 2 THEN
			UPDATE tbllogencuesta
            SET
				tbllogencuesta.logcteid = COALESCE(pcid, tbllogencuesta.logcteid),
				tbllogencuesta.logencuestaid = COALESCE(peid, tbllogencuesta.logencuestaid),
				tbllogencuesta.lognivelid = COALESCE(pnid, tbllogencuesta.lognivelid),
				tbllogencuesta.logavatarid = COALESCE(pavid, tbllogencuesta.logavatarid),
				tbllogencuesta.logaccesorioid = COALESCE(pacid, tbllogencuesta.logaccesorioid),
				tbllogencuesta.logpromoid = COALESCE(ppid, tbllogencuesta.logpromoid),
				tbllogencuesta.logiconopreguntaid = COALESCE(pipid, tbllogencuesta.logiconopreguntaid),
				tbllogencuesta.logpuntosacum = COALESCE(ppa, tbllogencuesta.logpuntosacum),
				tbllogencuesta.logfecha = COALESCE(pfe, tbllogencuesta.logfecha),
				tbllogencuesta.logevaluacion = COALESCE(pev, tbllogencuesta.logevaluacion),
				tbllogencuesta.loglocalizacion = COALESCE(plo, tbllogencuesta.loglocalizacion),
				tbllogencuesta.loglocalizacionmins = COALESCE(plm, tbllogencuesta.loglocalizacionmins)
			WHERE tbllogencuesta.logid = varid;
		WHEN vartipoaccion = 4 THEN
			SELECT * FROM tbllogencuesta
			WHERE tbllogencuesta.logid = varid;
			WHEN vartipoaccion = 5 THEN
				SELECT * FROM tbllogencuesta;
		END CASE;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_Promociones`(IN pta INT, IN pid INT, IN pemid INT, IN penid INT, IN pti VARCHAR(100), IN pdes TEXT, IN pfi DATETIME, IN pff DATETIME, IN pcq TEXT, IN pes TINYINT)
BEGIN
	DECLARE vartipoaccion 	INT;
	DECLARE varid 			INT;
	DECLARE varempid 		INT;
	DECLARE varencuestaid 	INT;
	DECLARE vartitulo 		VARCHAR(100);
	DECLARE vardescripcion 	TEXT;
	DECLARE varfechainicio 	DATETIME;
	DECLARE varfechafin 	DATETIME;
	DECLARE varcodigoqr 	TEXT;
    DECLARE varestado		TINYINT;

    SET vartipoaccion = pta;
	SET varid = pid;
	SET varempid = pemid;
	SET varencuestaid = penid;
	SET vartitulo = pti;
	SET vardescripcion = pdes;
	SET varfechainicio = pfi;
	SET varfechafin = pff;
	SET varcodigoqr = pcq;
    SET varestado = pes;

    CASE
		WHEN vartipoaccion = 1 THEN
			INSERT INTO tblpromocion
            VALUES(DEFAULT, varempid, varencuestaid, vartitulo, vardescripcion, varfechainicio, varfechafin, varcodigoqr, varestado);
		WHEN vartipoaccion = 2 THEN
			UPDATE tblpromocion
            SET
				tblpromocion.promoempid = COALESCE(pemid, tblpromocion.promoempid),
				tblpromocion.promoencuestaid = COALESCE(penid, tblpromocion.promoencuestaid),
				tblpromocion.promotitulo = COALESCE(pti, tblpromocion.promotitulo),
				tblpromocion.promodescripcion = COALESCE(pdes, tblpromocion.promodescripcion),
				tblpromocion.promofechainicio = COALESCE(pfi, tblpromocion.promofechainicio),
				tblpromocion.promofechafin = COALESCE(pff, tblpromocion.promofechafin),
				tblpromocion.promocodigoqr = COALESCE(pcq, tblpromocion.promocodigoqr)
			WHERE tblpromocion.promoid = varid;
        WHEN vartipoaccion = 3 THEN
			UPDATE tblpromocion
            SET tblpromocion.promoestado = 0
            WHERE tblpromocion.id = varid;
		WHEN vartipoaccion = 4 THEN
			SELECT * FROM tblpromocion
			WHERE tblpromocion.id = varid;
			WHEN vartipoaccion = 5 THEN
				SELECT * FROM tblpromocion;
		ELSE SELECT "ERROR";
	END CASE;
END$$
DELIMITER ;

DELIMITER $$
CREATE   PROCEDURE `SP_Usuarios`(IN pta INT, IN pid INT, IN ppn VARCHAR(100), IN psn VARCHAR(100), IN pap VARCHAR(100), IN pam VARCHAR(100), IN pcor VARCHAR(100), IN pcon VARCHAR(100), IN pdca VARCHAR(100), IN pdni NUMERIC(10), IN pdne NUMERIC(10), IN pdcol VARCHAR(100), IN pdmun VARCHAR(100), IN pdes VARCHAR(100), IN pdpa VARCHAR(100), IN pdcp NUMERIC(10), IN pfb TINYINT, IN pgm TINYINT, IN prec TINYINT, IN pfal DATETIME, IN pfba DATETIME, IN pes TINYINT)
BEGIN
	DECLARE vartipoaccion 		INT;
	DECLARE varid 				INT;
	DECLARE varprimernombre 	VARCHAR(100);
	DECLARE varsegundonombre 	VARCHAR(100);
	DECLARE varapellidopaterno 	VARCHAR(100);
	DECLARE varapellidomaterno 	VARCHAR(100);
	DECLARE varcorreo 			VARCHAR(100);
	DECLARE varcontrasena 		VARCHAR(100);
	DECLARE vardomcalle 		VARCHAR(100);
	DECLARE vardomnumeroint 	NUMERIC(10);
	DECLARE varcomnumeroext 	NUMERIC(10);
	DECLARE vardomcolonia 		VARCHAR(100);
	DECLARE vardommunicipio 	VARCHAR(100);
	DECLARE vardomestado 		VARCHAR(100);
	DECLARE vardompais 			VARCHAR(100);
	DECLARE vardomcp 			NUMERIC(10);
	DECLARE varbfacebook 		TINYINT;
	DECLARE varbgmail 			TINYINT;
	DECLARE varrecomendado 		TINYINT;
	DECLARE varfechaalta 		DATETIME;
	DECLARE varfechabaja 		DATETIME;
    DECLARE varestado			TINYINT;

    SET vartipoaccion = pta;
    SET varid = pid;
	SET varprimernombre = ppn;
	SET varsegundonombre = psn;
	SET varapellidopaterno = pap;
	SET varapellidomaterno = pam;
	SET varcorreo = pcor;
	SET varcontrasena = pcon;
	SET vardomcalle = pdca;
	SET vardomnumeroint = pdni;
	SET varcomnumeroext = pdne;
	SET vardomcolonia = pdcol;
	SET vardommunicipio = pdmun;
	SET vardomestado = pdes;
	SET vardompais = pdpa;
	SET vardomcp = pdcp;
	SET varbfacebook = pfb;
	SET varbgmail = pgm;
	SET varrecomendado = prec;
	SET varfechaalta = pfal;
	SET varfechabaja = pfba;
    SET varestado = pes;

    CASE
		WHEN vartipoaccion = 1 THEN
			INSERT INTO tblusuario
            VALUES (DEFAULT, varprimernombre, varsegundonombre, varapellidopaterno, varapellidomaterno, varcorreo, varcontrasena, vardomcalle, vardomnumeroint, varcomnumeroext, vardomcolonia, vardommunicipio, vardomestado, vardompais, vardomcp, varbfacebook, varbgmail, varrecomendado, varfechaalta, varfechabaja, varestado);
		WHEN vartipoaccion = 2 THEN
			UPDATE tblusuario
            SET
				tblusuario.usrprimernombre =	COALESCE(ppn, tblusuario.usrprimernombre),
				tblusuario.usrsegundonombre =	COALESCE(psn, tblusuario.usrsegundonombre),
				tblusuario.usrapellidopaterno =	COALESCE(pap, tblusuario.usrapellidopaterno),
				tblusuario.usrapellidomaterno =	COALESCE(pam, tblusuario.usrapellidomaterno),
				tblusuario.usrcorreo =	COALESCE(pcor, tblusuario.usrcorreo),
				tblusuario.usrcontrasena =	COALESCE(pcon, tblusuario.usrcontrasena),
				tblusuario.usrdomcalle =	COALESCE(pdca, tblusuario.usrdomcalle),
				tblusuario.usrdomnumeroint =	COALESCE(pdni, tblusuario.usrdomnumeroint),
				tblusuario.usrcomnumeroext =	COALESCE(pdne, tblusuario.usrcomnumeroext),
				tblusuario.usrdomcolonia =	COALESCE(pdcol, tblusuario.usrdomcolonia),
				tblusuario.usrdommunicipio =	COALESCE(pdmun, tblusuario.usrdommunicipio),
				tblusuario.usrdomestado =	COALESCE(pdes, tblusuario.usrdomestado),
				tblusuario.usrdompais =	COALESCE(pdpa, tblusuario.usrdompais),
				tblusuario.usrdomcp =	COALESCE(pdcp, tblusuario.usrdomcp),
				tblusuario.usrbfacebook =	COALESCE(pfb, tblusuario.usrbfacebook),
				tblusuario.usrbgmail =	COALESCE(pgm, tblusuario.usrbgmail),
				tblusuario.usrrecomendado =	COALESCE(prec, tblusuario.usrrecomendado),
				tblusuario.usrfechaalta =	COALESCE(pfal, tblusuario.usrfechaalta),
				tblusuario.usrfechabaja =	COALESCE(pfba, tblusuario.usrfechabaja)
			WHERE tblusuario.usrid = varid;
        WHEN vartipoaccion = 3 THEN
			UPDATE tblusuario
            SET tblusuario.usrestado = 0
            WHERE tblusuario.usrid = varid;
		WHEN vartipoaccion = 4 THEN
			SELECT * FROM tblusuario
			WHERE tblusuario.usrid = varid;
			WHEN vartipoaccion = 5 THEN
				SELECT * FROM tblusuario;
        ELSE SELECT "ERROR";
	END CASE;
END$$
DELIMITER ;
